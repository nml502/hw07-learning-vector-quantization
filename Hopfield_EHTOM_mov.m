% Pattern recognition with Hopfield network
% Demo with five chaaracter images
%
% Adapted from Ham and Kostanic, 2000
% for course COMP / ELEC / STAT 502, E. Merenyi
%
clear all
close all
%
% Specify the input characters as binary images
% Ideally, these should be read from a file, to make the code
% applicable to other image patterns. 
%
E=[1 1 1 1 1 1 1 0 0 0 0 0;
   1 1 1 1 1 1 1 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 1 1 1 1 0 0 0 0 0 0;
   1 1 1 1 1 1 0 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 0 0 0 0 0 0 0 0 0 0;
   1 1 1 1 1 1 1 0 0 0 0 0;
   1 1 1 1 1 1 1 0 0 0 0 0];

H=[0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 1 1 1 1 1 1 0 0;
   0 0 1 1 1 1 1 1 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0;
   0 0 1 1 0 0 0 0 1 1 0 0];
  
T=[0 1 1 1 1 1 1 1 1 1 1 0;
   0 1 1 1 1 1 1 1 1 1 1 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0;
   0 0 0 0 0 1 1 0 0 0 0 0];

zero=[0 0 0 1 1 1 1 1 1 0 0 0;
      0 0 1 1 1 1 1 1 1 1 0 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 1 1 1 0 0 0 0 1 1 1 0;
      0 0 1 1 1 1 1 1 1 1 0 0;
      0 0 0 1 1 1 1 1 1 0 0 0];

  M=[1 1 1 0 0 0 0 0 1 1 1 0;
   1 1 1 1 0 0 0 1 1 1 1 0;
   1 1 1 1 1 0 1 1 1 1 1 0;
   1 1 0 1 1 1 1 1 0 1 1 0;
   1 1 0 0 1 1 1 0 0 1 1 0;
   1 1 0 0 0 1 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0;
   1 1 0 0 0 0 0 0 0 1 1 0];
           % retrieving a pattern from memory
%
% Set pattern parameters
P = 5; % Number of input patterns
[xdim ydim] = size(E); % The x and y dimensions of image patterns
ndim = xdim * ydim; % The dimension of the vectorized patterns
%
%
maxtrial = 5; % set maximum number of recall iterations for 
%    
% Preprocess the inputs as needed. This could be in a function call.
%
EBP=ones(size(E));
HBP=ones(size(H));
TBP=ones(size(T));
zeroBP=ones(size(zero));
MBP=ones(size(M));
for i=1:12
   for j=1:12      
      if E(i,j)==0
         EBP(i,j)=-1;
      end
      if H(i,j)==0
         HBP(i,j)=-1;
      end
      if T(i,j)==0
         TBP(i,j)=-1;
      end
      if zero(i,j)==0
         zeroBP(i,j)=-1;
      end
      if M(i,j)==0
         MBP(i,j)=-1;
      end
   end
end 

% invert the B & W colors for better display
% I leave the original pixel assignments above as they were, for reference
EBP = -EBP;
HBP = -HBP;
TBP = -TBP;
zeroBP = -zeroBP;
MBP = -MBP;

% Plot characters here for verification
figure(1);
%title('Uncorrupted Inputs and Desired Outputs');
subplot(1,5,1), imagesc(EBP),colormap('gray')
subplot(1,5,2), imagesc(HBP),colormap('gray')
subplot(1,5,3), imagesc(TBP),colormap('gray')
title('Uncorrupted Inputs and Desired Outputs');
subplot(1,5,4), imagesc(zeroBP),colormap('gray')
subplot(1,5,5), imagesc(MBP),colormap('gray')
%set(gca,'xtick',[]), set(gca,'ytick',[]);

%saveas(gcf,'EHTOMuc.png');

%%
% "vec" each array to obtain a vectorized representation
% of the character, and group them into one mattrix of input patterns
%
EV=EBP(:);
HV=HBP(:);
TV=TBP(:);
zeroV=zeroBP(:);
MV=MBP(:);
ALL=[EV HV TV zeroV MV]; 
%
%%
%
% ================ From here, no data-specific chenges should be needed ==============
%              if one groups the patterns into one matrix and indexes them
%
% Form the Hopfield Synaptic Weight Matrix
W=zeros(ndim,ndim); 
% Build the Hopfield memory
for i=1:P
   W=W+ALL(:,i)*ALL(:,i)'; 
end
% Apply the normalization, and zero the diagonal elements
% of the weight matrix W (by subtracting P/N*(Identity matrix)
W=W/ndim-(P/ndim)*eye(ndim);


% Normalize the elements of the weight matrix to be in [0,1]
Wmin=min(min(W));
Wscaled=W-Wmin*ones(ndim,ndim);
Wmax=max(max(Wscaled));      
Wscaled=Wscaled/Wmax;


% Display the weight matrix
figure(2), image(Wscaled*63),colormap('gray')

%%
% Example of recalling one of the stored clean characters
% This is hard wired for recalling "E". Should be done for all training
% pattern (in an array, not individually) to check the quality
%
clear TESTV TESTM
TESTV=W*EV;
TESTV=sign(TESTV);
TESTM=reshape(TESTV,xdim,ydim);
min_TESTM=min(min(TESTM));
TESTM_scaled=TESTM-min_TESTM*ones(xdim,ydim);
max_TESTM=max(max(TESTM_scaled));      
TESTM_scaled=TESTM_scaled/max_TESTM;
%TESTM_scaled=invert(63*TESTM_scaled);
figure(3), imagesc(TESTM_scaled),colormap('gray')

%%
% Now corrupt a character, to input to recall
%
%[Xcorrupted,I] = noise(Xclean,BER); noise( ) corrupts elements of the pattern Xclean
%           in one column (1D noise with a Bit Error Rate equallto BER %). 
%           BER% noise means flipping BER% of of the pixels.
%[Xcorrupted,I,J] = noise2(Xclean,BER); noise2( ) corrupts elements in 2 dimansions (2D
%           noise with a Bit Error Rate equal to BER %)
%
% Set a corruption rate, and select the pattern to be corrupted and recalled
%
close all;
%
clear Xcorrupted
BER = 35;  %  
Xclean = EBP;
%Xclean = HBP; % can be perfect in a few steps, but often fails
%Xclean = TBP; 
%%Xclean = zeroBP;
%Xclean = MBP;
%
[Xcorrupted,I,J] = noise2(Xclean,BER);
clear TESTV TESTM
min_TESTM=min(min(Xcorrupted));
TESTM_scaled=Xcorrupted-min_TESTM*ones(xdim,ydim);
max_TESTM=max(max(TESTM_scaled));      
TESTM_scaled=TESTM_scaled/max_TESTM;
%TESTM_scaled=invert(63*TESTM_scaled);
%imagesc(TESTM_scaled),colormap('gray')
figure(4), imagesc(Xcorrupted),colormap('gray')
title(strcat('Corrupted input pattern BER=',num2str(BER),'%'));
%
%Save the corrupted image 
F(maxtrial+1) = struct('cdata',[],'colormap',[]); % allocate frames for saving figures
curFrame = 1;
F(curFrame) = getframe(gcf); curFrame=curFrame+1;
%%
% Recall the corrupted character
clear TESTV TESTM;
TESTV=Xcorrupted(:);
TESTM=0;
trial = 0;
%
while max(max(abs(TESTM-Xclean))) > 0 && trial < maxtrial %repeat until Actual = Desired
    TESTV=sign(W*TESTV); % This makes many state changes at once
    TESTM=reshape(TESTV,xdim,ydim);
    trial = trial+1;
    figure(50+trial), imagesc(TESTM),colormap('gray') % end of loop 
    title(strcat('Recalled pattern, step= ',num2str(trial),', input BER=',num2str(BER),'%'));
    F(curFrame) = getframe(gcf); curFrame=curFrame+1;
end 


%%
% Save figures in a movie
fig = figure; movie(fig,F,1,0.5); % movie(figure_handle,Movie_Frames,Replay_Count,FPS)
%%
% Make and save a video file of this movie
v = VideoWriter('OutPutVideo.avi');
open(v);
for  ii = 1:length(F)
  writeVideo(v,F(ii));
end
close(v);

