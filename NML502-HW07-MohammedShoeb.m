%%  Main driver
%   NML502-HW07-MohammedShoeb
%   Uses some code provided by Dr. Merenyi to class (in functions prob1 and
%   generate_data_prob1).
%
function hw07main
%% HEADER
%HW06, main function. Simply calls driver functions for problem1, problem1
%INPUT
%   none.
%RETURN
%   none.
%%
    clear all;
    osuffix = fix(clock);
    fsuffix  = sprintf(repmat('_%d', 1, size(osuffix, 2)),osuffix );
    
    %run and collect results for problem2a
    prob1(fsuffix);
    pause(2);
    close all;
   
    % run and collect results for problem2b and problem 3
    prob2(fsuffix);
    pause(2);
    close all;

    display('Finished running all problems and saved figures/log reports.');
    pause(2);
    fclose('all');

end

%% Driver for problem 1
function [W,total_iter] = prob1(fsuffix)
%Using some code provided by Dr. Merenyi to the class.

%   Setup configuration parameters.
    display('Starting problem 1');
    [ST,~] = dbstack;
    this_function = ST.name;
    [A,X,D,Atest,Xtest,~] = generate_data_prob1();
    [~,nx] = size(X);
    
    nP = 60;          % Number of LVQ prototypes (weight vectors)
    nC = 3;     
%     C  = randsample(1:nC,nP,true); % To assign labels randomly
%     % Here I assign labels by hand, divide them evenly among prototypes
    nLperClass = ceil(nP/nC); % This may not devide evenly into nP, take care
%     % The label assignment below is hard wired for the problem above, i.e., for 3
%     % classes. Generalize / extend for more classes.
    C = zeros(1,nP);
    C(1:nLperClass)=1; 
    C(nLperClass+1:2*nLperClass)=2;
    C(2*nLperClass+1:nP)=3;
    
    N              = 2e3;     % max. training epochs.
    log_events     = 1e3;     % total instants to log network parameters.
    log_step       = floor(N*nx/log_events);
    printW_step    = 5e1;     % plot prtotypes after these many logger calls.
    mu_init        = 0.5;
    mu_final       = 0.005;
    mu_decay_fn    = make_piecewise_step_decay([10000 20000 120000 inf],...
        [0.5 0.2 0.1 0.005]);
%     decay_fn2 = make_hyperbolic_decay(50*81);
    learning_rate_schedule = make_schedule(mu_decay_fn, @(x)(x), ....
        mu_init,mu_final); 
    tol            = 1;       % percentage classification error tolerance.
    error_function = make_lvq_classification_error_function(X,D,C);
    lvq_stop_predicate = make_stop_predicate(error_function,tol);
    
    %% nested functions: create logging and plotting functions for LVQ1
    lvq_logger_counter = 0;
    % allocate stores for learn history (recall).
    error_history  = zeros(1,1+log_events);
    learn_steps    = zeros(1,1+log_events);
%     F(1+log_events/printW_step) = struct('cdata',[],'colormap',[]);
    function lvq_logger(iter,W)
        err  = error_function(W);
        mu   = learning_rate_schedule('peek');

        for fid = [1, outfile]
            if(iter == 0)
                %Report initial network parameters
                fprintf(fid,'----------LVQ1 LEARN STARTED----------\n');
                fprintf(fid,'***Initial weights (cols: prototypes)***\n');
                print_matrix(fid,W);
                fprintf(fid,'***Learning rate***\n');
                print_matrix(fid,mu);
                fprintf(fid, ['***Stopping Criteria = max. learn_' ...
                    'steps OR error < tolerance***\n']);
                fprintf(fid,'\tMax learning steps\n');
                fprintf(fid,'\t%d\n',N*nx);
                fprintf(fid,'\tError Tolerance\n');
                fprintf(fid,'\t%g\n',tol);
                fprintf(fid,['***Initial Error = percentage samples '...
                    'misclassified***\n']);
                fprintf(fid,'\t%g\n',err);
                fprintf(fid,['***Performance indicators while running ' ...
                    'SOM algorithm***\n']);
            end
            fprintf(fid,'\titer: %-8d, error:%3.6f', iter,err);
            fprintf(fid,', mu: ');
            print_matrix(fid,mu);
        end
        
        if(mod(lvq_logger_counter,printW_step)== 0)
            fprintf(outfile,'\t****Weights(W)***\n');
            print_matrix(outfile,W);
            fig  = figure;
            tstr = sprintf('Prototypes, at learn step: %d',iter);
            fig  = diag_plot(fig,A,W,tstr);
            pause(0.1);
            saveas(gcf,sprintf('diagnostic_%sfig%d_%s.fig',...
                this_function,fig,fsuffix));
        end
        
        learn_steps(1 + iter/log_step)   = iter;
        error_history(1 + iter/log_step) = err;
%         F(lvq_logger_counter+1) = getframe(gcf);
        lvq_logger_counter = lvq_logger_counter+1;
    end

    % plotting function - not reuseable, need changes for other problems.
    label_color_map_r = [255;254;  0;252;255; 47;164;180];
    label_color_map_g = [  0;146;255;228;255;146;  0;180];
    label_color_map_b = [  0; 43;  0; 98;255;255;175;180];
    label_color_map   = [label_color_map_r label_color_map_g ...
        label_color_map_b]/255;

    function fig = diag_plot(fig,A,W,tstr)
    % nested function to create diagnostic plots. Used by logger.

       fprintf('\t|||||PLOTTING (classification map)|||||\n');
       fig = figure(fig);
       imagesc([1 9],[1 9], A), colormap(label_color_map([1 5 7],:)); 
       hold on;      
       plot(W(1,C==1),W(2,C==1),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(1,:),'MarkerSize',10);
       plot(W(1,C==2),W(2,C==2),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(5,:),'MarkerSize',10);
       plot(W(1,C==3),W(2,C==3),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(7,:),'MarkerSize',10);
       axis([0.5 9.5 0.5 9.5]);
       title(tstr);
    end

    %% create filenames/open output files, figures
    ofile    = strcat(this_function,'out',fsuffix,'.txt');
    outfile  = fopen(ofile,'wt');
    fprintf('Performance characteristics will be printed in file: %s\n', ...
        ofile);
    fprintf(['Performance graphs will be plotted in files with ' ....
        'suffices: %s\n'],fsuffix);

    fig  = figure;
    tstr = sprintf('Target classification region');
    fig  = diag_plot(fig,A,zeros(2,nx),tstr);
    saveas(gcf,sprintf('%sfig%d_before_training_class_map_%s.fig',...
        this_function,fig,fsuffix));
    
    %% learn using LVQ1
    [W,total_iter] = lvq3_learn(X,D,C,learning_rate_schedule,N,...
        @(W)(false),@lvq_logger,log_step);

    %% Gather results/statistics after training stopped.
    for file = [1, outfile]    
        fprintf(file,'----------AFTER TRAINING STOPPED----------\n');
        fprintf(file,'***Prototypes(W)***\n');
        print_matrix(file,W);
        fprintf(file,'***Learning Steps when training Stopped****\n');
        fprintf(file,'%d\n',total_iter);
        fprintf(file,'***Training Data: Error when training stopped***\n');
        fprintf(file,'%.6f\n',error_function(W));
    end

    Ahat = zeros(1,nx);
    for p = 1:nx
        m = best_match_node(W,X(:,p));
        Ahat(p) = C(m);
    end
    Ahat = reshape(Ahat,size(A));

    learn_steps = learn_steps(1:floor(total_iter/log_step)); 
    error_history = error_history(1:floor(total_iter/log_step)); 
    
    fig  = figure;
    tstr = sprintf(['Training Data: Classification region after training '...
        'and prototypes overlaid']);
    fig  = diag_plot(fig,Ahat,W,tstr);
    saveas(gcf,sprintf('%sfig%d_after_training_class_map_w_%s.fig',...
        this_function,fig,fsuffix));
    
    fig  = figure;
    tstr = sprintf('Training Data: Classification region after training');
    fig  = diag_plot(fig,Ahat,zeros(2,nx),tstr);
    saveas(gcf,sprintf('%sfig%d_after_training_class_map_%s.fig',...
        this_function,fig,fsuffix));
    
    fig = figure;
    fig  = diag_plot(fig,Ahat,W,tstr);
    tstr = sprintf(['Training Data: After training, voronoi '...
        'classification overlaid']);
    voronoi(W(1,:),W(2,:));
    saveas(gcf,sprintf('%sfig%d_after_training_voronoi_map_%s.fig',...
        this_function,fig,fsuffix));
    
    fig = figure;
    plot(learn_steps,error_history,'r-s','MarkerSize',2,'LineWidth',1);
    title('Learning History');
    grid on;
    xlabel('learn step');
    ylabel('percentage classification error');
    saveas(gcf,sprintf('%sfig%d_after_training_error_history_%s.fig',...
        this_function,fig,fsuffix));
    
    %% check performance on test data
    [mxtest,nxtest] = size(Xtest);
    Atest_hat = zeros(1,nxtest);
    for p = 1:nxtest
        m = best_match_node(W,Xtest(:,p));
        Atest_hat(p) = C(m);
    end
    Atest_hat = reshape(Atest_hat,size(Atest));
    
    test_error = Atest - Atest_hat;
    test_error(test_error~=0) = 1;
    test_error = 100*sum(test_error(:))/nxtest;
    
    
    for file = [1, outfile]    
        fprintf(file,'----------TEST DATA----------\n');
        fprintf(file,'***Test Data: Error when training stopped***\n');
        fprintf(file,'%.6f\n',test_error);
    end
    
   function fig = test_plot(fig,A,W,tstr)
    % nested function to create diagnostic plots. Used by logger.
       fig = figure(fig);
       imagesc([0.75 9.25],[0.75 9.25], A), colormap(label_color_map([1 5 7],:)); 
       hold on;      
       plot(W(1,C==1),W(2,C==1),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(1,:),'MarkerSize',10);
       plot(W(1,C==2),W(2,C==2),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(5,:),'MarkerSize',10);
       plot(W(1,C==3),W(2,C==3),'o','LineWidth',2.0,...
           'MarkerEdgeColor','k','MarkerFaceColor', ...
           label_color_map(7,:),'MarkerSize',10);
       axis([0.5 9.5 0.5 9.5]);
       title(tstr);
   end
    
    fig  = figure;
    tstr = sprintf('Test Data: Target classification map');
    fig  = test_plot(fig,Atest,zeros(2,nx),tstr);
    saveas(gcf,sprintf('%sfig%d_before_training_TEST_class_map_%s.fig',...
        this_function,fig,fsuffix));
    
    fig  = figure;
    tstr = sprintf(['Test Data: Classification region after training '...
        'and prototypes overlaid']);
    fig  = test_plot(fig,Atest_hat,W,tstr);
    saveas(gcf,sprintf('%sfig%d_after_training_TEST_class_map_w_%s.fig',...
        this_function,fig,fsuffix));
    
    fig  = figure;
    tstr = sprintf('Test Data: Classification region after training');
    fig  = test_plot(fig,Atest_hat,zeros(2,nx),tstr);
    saveas(gcf,sprintf('%sfig%d_after_training_TEST_class_map_%s.fig',...
        this_function,fig,fsuffix));
    
    fig = figure;
    fig  = test_plot(fig,Atest_hat,W,tstr);
    tstr = sprintf(['Test Data: After training, voronoi '...
        'classification overlaid']);
    voronoi(W(1,:),W(2,:));
    saveas(gcf,sprintf('%sfig%d_after_training_TEST_voronoi_map_%s.fig',...
        this_function,fig,fsuffix));
%     %%
%     % Save figures in a movie
%     fig = figure; movie(fig,F,1,0.5); 
%     %%
%     % Make and save a video file of this movie
%     v = VideoWriter(sprintf('movie_%sfig%d_%s.fig',...
%                 this_function,fig,fsuffix));
%     open(v);
%     for  ii = 1:length(F)
%       writeVideo(v,F(ii));
%     end
%     close(v);
    max(W(1,:)), min(W(1,:)), max(W(2,:)), min(W(2,:))
    display('Finished problem 4');    
end

%% Driver for problem 2
function prob2(fsuffix)
%% Header
%HW06, Problem 4: iris dataset
%Function to setup the parameters, train PCA network.
%INPUT
%   fsuffix: character string: file name suffix.
%RETURN
%   W : matrix: whose rows represnt the principal components.
%   total_iter : scalar: Number of learning steps when training stopped.
%%  
%   generate data, pre-process data, setup parameeters
    display('Starting problem 2');    
    [X, ~]  = generate_data_prob2();
    X       = detrend(X','constant')';    % now X has zero mean.
    
      % dure to rouding errors the assert below doesn't work
%     assert(isequal(0*mean(X,2),mean(X,2)), ['ERROR : prob2 : data is not' ...
%         ' zero mean']);

    [P,~,eig_valP,~] = pca(X','Centered',false);
    P = P';
    isI     = P*P';
    assert(~isequal(isI,eye(4)),'ERROR : prob2 : PP'' ~= I for P=pca(X)');
    
    [ST,~] = dbstack;
    this_function = ST.name;    
    [mx,nx]        = size(X);
    M              = mx;      % number of principal components.
    N              = 1e4;     % maximum learning epochs.    
    tol            = 1e-3;    % classification error percentage tolerance.
    log_events     = 1e3;     % total instants to log network parameters.
    printW_step    = 1e1;     % plot logs after these many calls to logger.
    log_step       = floor(N*nx/log_events);
    mu             = [5e-3;5e-3;5e-2;1e-1];   % learning rate.
    error_function = make_diff_error_function(eye(M));
    pca_stop_predicate = make_stop_predicate(error_function,tol);
    
    % allocate stores for learn history (recall).
    error_history  = zeros(1,1+log_events);
    W_history      = cell(1,M);
    for i=1:M
        W_history{i} = zeros(1+log_events,mx);
    end
    learn_steps    = zeros(1,1+log_events);
    WWt_diff_history = zeros(M,1+log_events);
       
    %% create filenames/open output files, figures
    ofile    = strcat(this_function,'out',fsuffix,'.txt');
    outfile  = fopen(ofile,'wt');
    fprintf('Performance characteristics will be printed in file: %s\n', ...
        ofile);
    fprintf(['Performance graphs will be plotted in files with ' ....
        'suffices: %s\n'],fsuffix);
    %% nested functions: create logging and plotting functions for PCA
    pca_logger_counter = 0;
    function pca_logger(iter,W,mu)
    %nested function to recall and record diagnostics while learning.
    %Passed as a function handle to som_learn.
    %INPUT
    %   iter : scalar: current iteration counter (iteration = learn step).
    %   W    : matrix: whose columns are current prototypes.
    %RETURN
    %   none.
        err  = error_function(W);
        mu   = mu';
        WWt_diff = abs(W*W'-eye(M));

        for fid = [1, outfile]
            if(iter == 0)
                %Report network parameters
                fprintf(fid,'----------PCA LEARN STARTED----------\n');
                fprintf(fid,'***Initial weights (cols : vectors)***\n');
                print_matrix(fid,W);
                fprintf(fid,'***Learning rate***\n');
                print_matrix(fid,mu);
                fprintf(fid, ['***Stopping Criteria = max. learn_' ...
                    'steps OR error < tolerance***\n']);
                fprintf(fid,'\tMax learning steps\n');
                fprintf(fid,'\t%d\n',N*nx);
                fprintf(fid,'\tError Tolerance\n');
                fprintf(fid,'\t%g\n',tol);
                fprintf(fid,'***Initial Error = max(abs(WW''-I))***\n');
                fprintf(fid,'\t%g\n',err);
                fprintf(fid,['***Performance indicators while running ' ...
                    'SOM algorithm***\n']);
                fprintf(fid,'\titer: %-8d, error:%3.6f', iter,err);
                fprintf(fid,', mu: ');
                print_matrix(fid,mu);
            else
                fprintf(fid,'\titer: %-8d, error:%3.6f', iter,err);
                fprintf(fid,', mu: ');
                print_matrix(fid,mu);
            end
        end
        
        if(mod(pca_logger_counter,printW_step)== 0)
            fprintf(outfile,'\t****Weights(W)***\n');
            print_matrix(outfile,W);
            fprintf(outfile,'\t****abs(W * W'' - I)***\n');
            print_matrix(outfile,WWt_diff);
            fig  = figure;
            tstr = sprintf('%s, at learn step: %d','W*W'' ',iter);
            fig  = diag_plot(fig,W,tstr);
            pause(1);
            saveas(gcf,sprintf('diagnostic_fig%d_%s.fig',fig,fsuffix));
        end
        
        learn_steps(1 + iter/log_step)   = iter;
        error_history(1 + iter/log_step) = err;
        for ii=1:M
            W_history{ii}(1+iter/log_step,:) = W(ii,:);
        end
        WWt_diff_history(:,1+iter/log_step) = diag(WWt_diff);
        pca_logger_counter = pca_logger_counter+1;
    end
                
    function fig = diag_plot(fig,W,tstr)
    % nested function to create diagnostic plots. Used by logger.
        fig = figure(fig);
        colormap(gray);
        imagesc(abs(W*W'-ones(M)));
        title(tstr);
    end
        
    %% pca analysis
    [W,total_iter] = pca_learn(X, M, mu, N, pca_stop_predicate, ...
        @pca_logger, log_step);
    
    %% Gather results/statistics after training stopped.
    learn_steps = learn_steps(1:floor(total_iter/log_step)); 
    error_history = error_history(1:floor(total_iter/log_step)); 
    for j = 1:M
        W_history{j} = W_history{j}(1:floor(total_iter/log_step),:);
    end
    WWt_diff_history = WWt_diff_history(:,1:floor(total_iter/log_step));
    PW_corr = sum(P.*W,2);   %correlate MATLAB's pca and Sangers
    
    %% Report and plot results. 
    %  -- this section is probably not resusable code.
    for file = [1, outfile]    
        fprintf(file,'----------AFTER TRAINING STOPPED----------\n');
        fprintf(file,'****Principal Components from MATLAB''s pca command***\n');
        print_matrix(file,P);
        fprintf(file,'****Eigenvalues from MATLAB''s pca command***\n');
        print_matrix(file,eig_valP);
        fprintf(file,'***Weights(W)***\n');
        print_matrix(file,W);
        fprintf(file,['***principal components correlation between '...
            'MATLAB''s pca and Sanger''s (expected 1 or -1)***\n']);
        print_matrix(file,PW_corr');
        fprintf(file,'****abs(W * W'' - I)***\n');
        print_matrix(file,abs(W*W'-eye(M)));
        fprintf(file,'***Learning Steps when training Stopped****\n');
        fprintf(file,'%d\n',total_iter);
        fprintf(file,'***Error when training stopped***\n');
        fprintf(file,'%.6f\n',error_function(W));
    end
    
    fg1 = figure;
    tstr = 'Problem 2.2 : Error History, error = max(abs(W*W^T - I))';
    plot(learn_steps,error_history,'color','r','markers',2,'LineWidth',2);
    grid on;
    title(tstr);
    xlabel('Learning Step');
    ylabel('max(abs(W*W^T - I))');
    saveas(gcf,sprintf(['%sfig%d_after_training_error_history_'...
        '%s.fig'],this_function,fg1,fsuffix));
    
    for i=1:M
        fg2 = figure;
        title_str = sprintf('Principal Component %d',i);
        for j = 1:mx
            hold on;
            plot(learn_steps,W_history{i}(:,j),'-k*','markers',2,...
                 'LineWidth',1);
        end
        title(title_str);
        xlabel('Learning Step');
        grid on;
        saveas(gcf,sprintf(['%sfig%d_after_training_principal_'...
            'component_%s.fig'],this_function,fg2,fsuffix));
    end
    
    fg3=figure;
    leg=cell(1,M);
    col = {[1 0 0], [0 0.5 0], [0 0 1], [0 0 0]};
    for i=1:M
        hold on;
        plot(learn_steps,WWt_diff_history(i,:),'color',col{i},...,
            'markers',2,'LineWidth',2);
        leg{i}=sprintf('diagonal element=%d',i);
    end
    
    title('diag[abs((W x W^T - I))]');
    legend(leg);
    grid on;
    xlabel('Learning Step');
    saveas(gcf,sprintf(['%sfig%d_after_training_diagonal_'...
        'diff_%s.fig'],this_function,fg3,fsuffix));
        
    display('Finished problem 4');
end

%% lvq_learn function
function [W,iter] = lvq3_learn(X,D,C,eta,N,stop_predicate,lvq_logger, ...
    log_step)
%% HEADER
%Run Kohonen LVQ3 learning algorithm.
%
%INPUT
%   X : matrix: Input training data. Columns of this matrix correspond to 
%       input vectors.
%   D : vector: class labels for corresponding inputs.
%   C : vector: class labels of prototypes(see W below).
%
%   eta: function handle: Should take one argument, as eta('message')
%        message = reset means initialize eta.
%        message = next means move and get next learning rate per schedule.
%        message = peek means get current learning rate (but don't move).
%
%   N : scalar: maximum learning epochs for the algorithm to terminate.
%       epoch = number of data samples, known from size of X(see above).
%       max learn steps = N * number of data samples.
%
%   stop_predicate: function handle: predicate to indicate if learning
%                   stops for current weights 'W' before max learn steps.
%                   It should take one argument, as in stop_predicate(W).
%
%   lvq_logger : function handle: callback function to report diagnostics.
%                It should take two parameters, as in lvq_report(iter,W).
%                iter = current learning step, W    = curretn weights
%
%   log_step : scalar: callback 'lvq_logger' every 'log_step' learn steps.
%
%RETURN
%   W : matrix: whose columns represnt the prototypes. The columns
%   correspond to class labels in C.
%
%   iter : scalar: number of learning steps (when learning stopped).
%%
    % pre-process input, initialize weights, other parameters.
    [mx,nx] = size(X);            % its better if pre-processing type is 
%     X_mean = mean(X,2);           % also a parameter for lvq_learn.
%     X      = bsxfun(@minus, X,X_mean);
%     X_scale= max(abs(X(:)));
%     X      = bsxfun(@rdivide, X,X_scale);% X is now normalized input

    M = length(C);
    data_min=min(min(X));
    data_max=max(max(X));
    % Initialize the weights in the range of the input data
    scale=1; % Make this a parameter if you like
    W=scale*(rand(mx,M)*(data_max - data_min)+data_min);
    
%     W = 2*rand(mx,M,'double')-1;         % initialialize weights.
%     W_denormalized = bsxfun(@times,W,X_scale);
%     W_denormalized = bsxfun(@plus,W_denormalized,X_mean);
%
%     input X pre-processing and thus W_denormalized is commented because
%     not using them here.

    mu=eta('reset');                     % initialize learning rate.    
    iter = 0;                            % total iterations counter.
    
    for epoch = 1:N*nx     % repeat and learn until convergence or max. steps.                
            p = randsample(1:nx,1);
            %1. stop learning if true.
%             if(stop_predicate(W_denormalized))
            if(stop_predicate(W))
%                 W = W_denormalized; 
                lvq_logger(iter,W);
                return;
            end
            
            % book-keeping (call logger)
            if(mod(iter,log_step)==0)
                lvq_logger(iter,W);      
            end   
            
            %2. find best match
            Q = bsxfun(@(w,x)((w-x).^2),W,X(:,p));  % Euclidean distance
            Q = sum(Q,1);
            [minq,ci] = min(Q);
            [~,si] = min(Q(Q>minq));
            center = mean([W(:,ci),W(:,si)],2);
            dist_cs = norm(W(:,ci)-W(:,si));
%             [~,ci] = min(sum(Q,1));
            
            %3. update weights.
    		if (C(ci) == D(p))
                W(:,ci) = W(:,ci) + mu * (X(:,p)-W(:,ci));
                if(C(si)==D(p) && norm(X(:,p)-center)<= 0.25*dist_cs)
                    % the 0.25 here can be made a parameter.
                    W(:,si) = W(:,si) + 0.25*mu*(X(:,p)-W(:,si));
                end                
            else
                W(:,ci) = W(:,ci) - mu * (X(:,p)-W(:,ci));
                if(C(si)==D(p) && norm(X(:,p)-center)<= 0.25*dist_cs)
                    W(:,si) = W(:,si) + mu*(X(:,p)-W(:,si));
                end
            end
%             W_denormalized = bsxfun(@times,W,X_scale);
%             W_denormalized = bsxfun(@plus,W_denormalized,X_mean);            
            
            %4. next iteration, learning rate and neighborhood function.
            mu = eta('next');
            iter = iter+1;         
%         end
    
    end

%     W = W_denormalized;
end

%% pca_learn function
function [W,iter] = pca_learn(X,M,mu,N,stop_predicate,pca_logger, ...
    log_step)
%% HEADER
%Run Sanger's PCA algorithm.
%
%INPUT
%   X : matrix: Input training data. Columns of this matrix correspond to 
%       input vectors.
%   M : scalar: Total number of copoments (m <= dimension data vectors)
%
%   stop_predicate: function handle: predicate to indicate if learning
%                   stops for current weights 'W' before max learn steps.
%                   It should take one argument, as in stop_predicate(W).
%                   W = current weights.
%
%   mu: vector: Learning rate (for all principal components).
%
%   N : scalar: maximum learning epochs for the algorithm to terminate.
%       epoch = number of data samples, known from size of X(see above).
%       max learn steps = N * number of data samples.
%
%   som_logger : function handle: callback function to report diagnostics.
%                It should take two parameters, as in som_report(iter,W).
%                iter = current learning step
%                W    = current weights
%
%   log_step : scalar: callback 'som_logger' every 'log_step' learn steps.
%
%RETURN
%   W : matrix: whose rows represnt the principal component vectors. 
%
%   iter : scalar: number of learning steps (when learning stopped).
%%
    % initialize weights, other parameters.
    [mx,nx] = size(X);
    W       = 2*rand(M,mx,'double')-1;   % initialialize weights.
%     mu      = Inf*ones(M,1);             % initialize learning rate.    
%     mu = mu*ones(M,1);                   % initialize learning rate.
%     mu(M) = 2;
    iter    = 0;                         % learning step counter.
    
    for epoch = 1:N     % repeat and learn until convergence or max. steps.                
        for p = randperm(nx)
            % book-keeping (call logger)
            if(mod(iter,log_step)==0)
                pca_logger(iter,W,mu);      
            end
            
            %1. stop learning if true.
            if(stop_predicate(W))
                break;
            end
 
            %2. update weights.            
            y  = W*X(:,p); 
%             mu = 1./((gamma./mu) + y.^2);
            W  = W + bsxfun(@times,(y*(X(:,p)') - tril(y*y')*W),mu);
            
            %3. next iteration.
            iter = iter+1;        
        end
    end
end

%% Classfication error function - Not used in this m-file.
function ef = make_lvq_classification_error_function(X,D,C)
%% HEADER
%Evaluate classification error given prototypes W, input X, and desired
%output D. First step, use majority rule to assign a node on SOM lattice to
%a class. On second step, run the input X agaain, and count classfication
%errors.
%INPUT
%   W     : matrix: whose columns are current prototypes.
%   X     : matrix: whose columns are input data samples.
%   D     : vector: class labels corresponding to X.
%RETURN
%   perr  : scalar: percentage of misclassified samples. 
%%
%
    nX = size(X,2);
      
    function error = lvq_classification_error_function(W)
        error = 0;

        for p = 1:nX
            m = best_match_node(W,X(:,p));
            error = error + (C(m)~= D(p));
        end
        error = 100*error/nX; 
    end
    ef = @lvq_classification_error_function;
end

%% factory method for error function (used for homework7 problem2)
 function ef = make_diff_error_function(I)
%% Header
%INPUT
%   I  : matrix 
%RETURN
%   ef : function handle: ef(W) returns abs(I-W*W')
%
    ef = @(W)(max(max(abs(I-W*W'))));
 end

%% factory method for stopping predicate, used by lvq3_learn, pca_learn
function pred = make_stop_predicate(ef, tol)
%%
%factory method for stopping predicate, used by som_learn
%INPUT
%   ef : function handle: takes one argument W.
%   tol: scalar: acceptable error tolerance.
%RETURN
%   pred : function handle: predicate.
%
    function stop = stop_predicate(W)
        error = ef(W);
        stop = all(error(:)<=tol);
    end

    pred = @stop_predicate;
end

%% factory method for piecewise step decay
function decay_fn = make_piecewise_step_decay(T,V)
%% HEADER
%factory method for picewise step decay generator.
%   T       : vector: times at which steps happen.
%   V       : vector: values desired at those times.
%RETURN
%   decay_fn : function handle: generates piecewise decaying step function.
%%
    myclock = make_discrete_time_clock();
    V       = V/max(abs(V));
    V_begin  = V(1);
    V_end   = V(end);
    scale   = 1/(V_begin-V_end);
    shift   = 0 - scale*V_end;
    V = scale*V + shift;
    
    function decay = piecewise_step_decay(message)
        t = myclock(message);
        i = find(T-t>=0,1);
        decay = V(i);
    end
    
    decay_fn = @piecewise_step_decay;
end

%% factory method for hyperbolic time decay
function decay_fn = make_hyperbolic_decay(T)
%% HEADER
%factory method for Hyperbolic time decay series generator.
%   T       : scalar: time scaling factor.
%RETURN
%   decay_fn : function handle: generates 1/(1+t/T), whre t is discrete.
%              decay_fn values go from 1 to 0.
%%
    myclock = make_discrete_time_clock();
    decay_fn = @(message)(1 / (1 + myclock(message)/T));
end
    
%% factory method for discrete time series (clock)
function dicrete_clock = make_discrete_time_clock()
%% HEADER
%factory method for discrete time series generator (clock).
%The clock has includes simple error recovery.
%On error, this m-file stops. The user may then choose to exit/continue.
%INPUT
%   T       : scalar: time scaling factor.
%RETURN
%   discrete_clock : function handle: generates 1/(1+t/T), whre t is discrete.
%%
    time = 0;     
    function discrete_time = inner_fn(message)
    %Input:
    %   message : character string: From the set {'reset','next','peek'}.
    %             If message = 'reset' ,reset time and return decay factor
    %             message = 'next' ,move time and retrun decay_factor.
    %             message = 'peek' ,return decay_factor at current time.
    %Return:
    %   discrete_time: scalar: discrete time starting from 0,1,2...
        if(isempty(time)) 
            time = 0;             % initialize.
        end
    
        switch message
            case 'reset'
                time = 0;         % initialize.
            case 'next'
                time = time+1;    % move one time step.
            case 'peek'           % do nothing.
            otherwise             % error handling.
                disp('ERROR : discrete_clock');
                disp('wrong argument passed for formal parameter message');
                disp('\t Expecting message = ''reset'' or ''next'' or ''peek'' ');
                fprintf('\t Got message = ''%s''\n', message);
                disp('You may choose to end the program');
                disp('\t<a href="MATLAB: dbquit;">Yes</a> / <a href="MATLAB: dbcont;">No</a>')
                keyboard;
        end
        discrete_time = time;
    end

    dicrete_clock = @inner_fn;
end

%% factory method for making schedule
function schedule = make_schedule(df,tf,init,final)
%% HEADER
%INPUT
%   df    : function handle: decay function (assume decay function values
%                            go from 1 to 0)
%   tf    : function handle: transform (for example rounding etc...)
%   init  : scalar: initial value.
%   final : scalar: initial value.
%RETURN
%   schedule : function_handle: A schedule start from 'init' to 'final'
%              decaying as 'df'. Then, transform 'tf' is applied.
    schedule = @(message)(tf((init-final)*df(message)+final));
end

%% Helper function to find best match node
function match = best_match_node(W,x)
%% HEADER
%Find and return best matching node index using L2 norm distance.
%INPUT
%   W     : matrix: whose columns are current prototypes.
%   x     : vector: data sample.
%RETURN
%   match : scalar: index of node with best match. 
%%
    Q     = bsxfun(@(w,x)((w-x).^2),W,x);  % L2 norm for distance
    [~,match] = min(sum(Q,1));
%         nP = size(W,2);
%   		d = zeros(1,nP);
%    		for j = 1:nP
%       		d(j) = norm(W(:,j)-x);
%    		end
%    		[mindist,match] = min(d);
end

%% Helper for logging function to print current set of prototypes
function print_matrix(ofile,W)
%% HEADER
%Function to print current set of weights ot ofile.
%Used as a helper function for printing weights. For example, this is used
%by the function som_report).
%INPUT
%   ofile : scalar: output file (weights will be printed to this file)
%                   It is assumed to be open.
%   W     : matrix: whose columns are current prototypes.
%RETURN
%   none. No return value (this function only has side effects).  
%%
    fprintf(ofile,[repmat('\t%+8.6f ',1,size(W,2)) '\n'],W');             
end

%% Data generator for problem 1
function [A,X,Cx,Atest,Xtest,Cxtest] = generate_data_prob1()
%% HEADER
%Copied from code provided by Dr. Merenyi.
%Generate datasets for homework7 problem1.
%
%INPUT
%   npoints : scalar: number of data points in each dataset.
%RETURN
%   X : matrix: columns are data samples.
%   Cx : vector: class labels {1,2,,3}
%% 
    % Specify the matrix of class labels for the spatial locations
	A = [ 3 1 1 1 1 2 3 3 3
   	3 3 3 3 3 2 3 3 3
   	3 3 3 3 3 1 2 2 2
   	3 1 1 1 1 1 1 1 1
   	3 1 1 1 2 1 1 1 1
   	3 1 1 2 2 2 1 1 1 
   	3 1 2 2 1 3 2 2 2
   	3 3 3 3 3 3 2 2 2
   	3 3 3 3 3 3 3 3 2];

    [nrows,ncols]=size(A);
    N = nrows*ncols;
    ndim = 2;
    
    Atest = kron(A,ones(2,2));
    [nrows_test,ncols_test] = size(Atest);
    Ntest = nrows_test*ncols_test;

    % Generate the training data:
	X = zeros(ndim,N); 
	Cx = zeros(1,N);
	k = 0;
	for i = 1:nrows
        for j = 1:ncols
            k = k+1;
%       X(1,k) = i-ceil(nrows/2); % to center the y coordinates if desired
%      	X(2,k) = j-ceil(ncols/2); % to center the x coordinates if desired
            X(1,k) = i;
            X(2,k) = j;
%      	Cx(k) = A(ncols-j+1, i); % this seems to flip
                                 % then requires a flipud at the end
            Cx(k) = A(j,i);
        end
    end

    % Generate the test data:
	Xtest = zeros(ndim,Ntest); 
	Cxtest = zeros(1,Ntest);
	k = 0;
	for i = 1:nrows_test
        for j = 1:ncols_test
            k = k+1;
            Xtest(1,k) = 0.75 + (i-1)*0.5;
            Xtest(2,k) = 0.75 + (j-1)*0.5;
            Cxtest(k) = Atest(j,i);
        end
    end   
end

%% Data generator for problem 2
function [X, D] = generate_data_prob2
%% HEADER
%Generate dataset for homework7 problem2 (iris dataset).
%INPUT
%   no input (however, the iris data ascii files must be present in same
%   directory as this m file).
%RETURN
%   [X,D] = input vectors in matrix 'X' column-wise and corresponding 
%           class labels in vector 'D'
%%
%
    ftrain = fopen('iris-train.txt','rt');
%     ftest  = fopen('iris-test.txt','rt');

    persistent train_data;
%     persistent test_data;

    if(isempty(train_data))
        display(['reading iris training dataset from file iris-train.txt'...
            'for Homework6, problem4']);
        train_data = textscan(ftrain,'%f%f%f%f\n&%f%f%f\n','HeaderLines',8);
    end
%     if(isempty(test_data))
%         display(['reading iris training dataset from file iris-test.txt'...
%             'for Homework6, problem4']);
%         test_data = textscan(ftest,'%f%f%f%f\n&%f%f%f\n','HeaderLines',8);
%     end

    xtrain = [train_data{1}, train_data{2},train_data{3}, train_data{4}]';
    dtrain = [train_data{5}, train_data{6}, train_data{7}]';
%     xtest  = [test_data{1}, test_data{2},test_data{3}, test_data{4}]';
%     dtest  = [test_data{5}, test_data{6}, test_data{7}]';

    X = [xtrain];
    D = [dtrain];
    [~,D] = max(D,[],1);    %class labels

end